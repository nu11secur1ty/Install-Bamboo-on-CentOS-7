![image](https://github.com/nu11secur1ty/Install-Bamboo-on-CentOS-7/blob/master/photo/Bamboo_Logo.png)


# Install Bamboo on CentOS 7
Bamboo is a continuous integration and deployment server. It provides an automated and reliable build/test process for software source-codes. It is an efficient way to manage the build that have different requirements. The build and test processes are triggered automatically on completion of the code. It provides sophisticated methodology for the Software development teams as:

a) An automated building and testing of software source-code
b) Providing updates on successful and failed builds
c) Reporting tools for statistical Analysis
d) Build information

# System Requirements for the installation
# Hardware Considerations:

   1 The Software only supports 64 bit derived hardware platforms.
   2 The CPU/RAM depends upon the complexity of the plans. For a minimum installation setup I recommend atleast 4 core CPU and 2GB RAM
   3 20GB storage is the minimum requirement for the installation

# Software Considerations:

     1.Bamboo requires a full Java Development Kit (JDK) platform to be installed on the server. 
       It's purely a Java application and run on any platforms provided all the Java requirements are satisfied.
     2 It is a Web application, hence needs an application server. Tomcat is the application server used for this.
     3 It supports almost all popular relational database servers like PostgreSQL, MySQL, Oracle, MicroSoft SQL server etc

In this article, I'm providing the guidelines for the installation of this Web Application on a CentOS 7 server. Let's walk through the installation steps.

# 1. Check the supported platforms

As mentioned above, you can check and confirm the availability of the system requirements including the hardware and software considerations.

# 2. Check the Java version

This application requires the JDK 1.8 version to be installed on the server. If you've not installed this. Then make sure you download and install this exact JDK version as required.


```
    [root@server1 kernels]#yum install java-1.8.0-openjdk

    Dependencies Resolved

    ==========================================================================================================================
    Package Arch Version Repository Size
    ==========================================================================================================================
    Installing:
    java-1.8.0-openjdk x86_64 1:1.8.0.91-0.b14.el7_2 updates 219 k
    Installing for dependencies:
    alsa-lib x86_64 1.0.28-2.el7 base 391 k
    fontconfig x86_64 2.10.95-7.el7 base 228 k
    fontpackages-filesystem noarch 1.44-8.el7 base 9.9 k
    giflib x86_64 4.1.6-9.el7 base 40 k
    java-1.8.0-openjdk-headless x86_64 1:1.8.0.91-0.b14.el7_2 updates 31 M
    javapackages-tools noarch 3.4.1-11.el7 base 73 k
    libICE x86_64 1.0.9-2.el7 base 65 k
    libSM x86_64 1.2.2-2.el7 base 39 k
    libXext x86_64 1.3.3-3.el7 base 39 k
    libXfont x86_64 1.5.1-2.el7 base 150 k
    libXi x86_64 1.7.4-2.el7 base 40 k
    libXrender x86_64 0.9.8-2.1.el7 base 25 k
    libXtst x86_64 1.2.2-2.1.el7 base 20 k
    libfontenc x86_64 1.1.2-3.el7 base 30 k
    lksctp-tools x86_64 1.0.13-3.el7 base 87 k
    python-javapackages noarch 3.4.1-11.el7 base 31 k
    python-lxml x86_64 3.2.1-4.el7 base 758 k
    ttmkfdir x86_64 3.0.9-42.el7 base 48 k
    tzdata-java noarch 2016d-1.el7 updates 179 k
    xorg-x11-font-utils x86_64 1:7.5-20.el7 base 87 k
    xorg-x11-fonts-Type1 noarch 7.5-9.el7 base 521 k

    Transaction Summary
    =========================================================================================================================
    Install 1 Package (+21 Dependent packages)

    Total download size: 34 M
    Installed size: 110 M

    [root@server1 kernels]# echo $JAVA_HOME
    [root@server1 kernels]# java -version
    openjdk version "1.8.0_91"
    OpenJDK Runtime Environment (build 1.8.0_91-b14)
    OpenJDK 64-Bit Server VM (build 25.91-b14, mixed mode)
```

# 2. Install PostgreSQL


Bamboo installation choose PostgreSQL database by default. Install this if you plan to use this database server for this application. You can even use other external databases like MySQL, but you need to connect the application to this external database in that case. JDBC driver for PostgreSQL is bundled with the Bamboo installation. But for any other external application we need to configure Bamboo JDBC connection to the external database. I've choosen to use PostgreSQL as my database server. I've run this command to install this.


```
root@server1 ~]# yum install postgresql
```


# 3. Creating the application user and managing installation/application folders.


It is always recommended to run an application as its dedicated user rather than as root. I created a user to run this application and also created an application data and installation folder prior to the installation. I changed the ownerships of the folders to the dedicated bamboo user created.


```
root@server1 kernels]# useradd --create-home -c "Bamboo role account" bamboo
[root@server1 bamboo]# mkdir -p /opt/atlassian/bamboo
[root@server1 bamboo]# chown bamboo: /opt/atlassian/bamboo
[root@server1 bamboo]# ls -ld /opt/atlassian/bamboo
drwxr-xr-x 2 bamboo bamboo 4096 Apr 26 05:26 /opt/atlassian/bamboo
```


Now you can switch to the bamboo user and download the Bamboo installation packages from their website and extract that in the installation folder.

```
    root@server1 bamboo]# su - bamboo
    [bamboo@server1 ~]$ cd /opt/atlassian/bamboo
    [bamboo@server1 bamboo]$

    [bamboo@server1 tmp]$ wget https://www.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-5.10.3.tar.gz
    --2016-04-26 05:28:54-- https://www.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-5.10.3.tar.gz
    Resolving www.atlassian.com (www.atlassian.com)... 52.87.106.229, 54.86.154.79
    Connecting to www.atlassian.com (www.atlassian.com)|52.87.106.229|:443... connected.
    HTTP request sent, awaiting response... 301 Moved Permanently
    Location: https://my.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-5.10.3.tar.gz [following]
    --2016-04-26 05:28:55-- https://my.atlassian.com/software/bamboo/downloads/binary/atlassian-bamboo-5.10.3.tar.gz
    Resolving my.atlassian.com (my.atlassian.com)... 131.103.28.9
    Connecting to my.atlassian.com (my.atlassian.com)|131.103.28.9|:443... connected.
    HTTP request sent, awaiting response... 302 Found
    Location: https://downloads.atlassian.com/software/bamboo/downloads/atlassian-bamboo-5.10.3.tar.gz [following]
    --2016-04-26 05:28:55-- https://downloads.atlassian.com/software/bamboo/downloads/atlassian-bamboo-5.10.3.tar.gz
    Resolving downloads.atlassian.com (downloads.atlassian.com)... 72.21.81.96
    Connecting to downloads.atlassian.com (downloads.atlassian.com)|72.21.81.96|:443... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 214301412 (204M) [application/x-gzip]
    Saving to: ‘atlassian-bamboo-5.10.3.tar.gz’

    100%[=====================================================================================================>] 214,301,412 62.1MB/s in 3.4s

    2016-04-26 05:28:58 (61.0 MB/s) - ‘atlassian-bamboo-5.10.3.tar.gz’ saved [214301412/214301412]

    [bamboo@server1 tmp]$ cd -
    /opt/atlassian/bamboo
    [bamboo@server1 bamboo]$
    [bamboo@server1 bamboo]$ tar -xvf /tmp/atlassian-bamboo-5.10.3.tar.gz
```

Create a symlink to a directory current for the ease of managing the files.

```
[bamboo@server1 bamboo]$ ln -s atlassian-bamboo-5.10.3 current
[bamboo@server1 bamboo]$ ll
total 4
drwxr-xr-x 13 bamboo bamboo 4096 Mar 14 14:47 atlassian-bamboo-5.10.3
lrwxrwxrwx 1 bamboo bamboo 23 Apr 26 05:30 current -> atlassian-bamboo-5.10.3
```


Now create and modify the application-data folder location in the Bamboo configuration files.


```
    [root@server1 bamboo]# mkdir -p /var/atlassian/application/bamboo
    [root@server1 var]# chown bamboo: /var/atlassian/application/bamboo/
    [bamboo@server1 bamboo]$ cat current/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties
    ## You can specify your bamboo.home property here or in your system environment variables.

    #bamboo.home=C:/bamboo/bamboo-home
    bamboo.home=/var/atlassian/application/bamboo
```


It is recommended to keep different folder locations for the installation and storage of this application.

# 4. Start Bamboo

Now you switch to the bamboo user and move to your installation folder. Run the startup script from the installation folder.

```
    bamboo@server1 current]$ pwd
    /opt/atlassian/bamboo/current

    [bamboo@server1 current]$ bin/start-bamboo.sh

    To run Bamboo in the foreground, start the server with start-bamboo.sh -fg

    Server startup logs are located in /home/bamboo/current/logs/catalina.out

    Bamboo Server Edition
    Version : 5.10.3

    If you encounter issues starting or stopping Bamboo Server, please see the Troubleshooting guide at https://confluence.atlassian.com/display/BAMBOO/Installing+and+upgrading+Bamboo

    Using CATALINA_BASE: /home/bamboo/current
    Using CATALINA_HOME: /home/bamboo/current
    Using CATALINA_TMPDIR: /home/bamboo/current/temp
    Using JRE_HOME: /
    Using CLASSPATH: /home/bamboo/current/bin/bootstrap.jar:/home/bamboo/current/bin/tomcat-juli.jar
    Tomcat started.

    [bamboo@server1 current]$ tail -f /home/bamboo/current/logs/catalina.out
    2016-04-26 07:42:38,834 INFO [localhost-startStop-1] [lifecycle] * Bamboo is starting up *
    2016-04-26 07:42:38,834 INFO [localhost-startStop-1] [lifecycle] *******************************
    2016-04-26 07:42:38,835 INFO [localhost-startStop-1] [ServletContextHolder] Setting servlet context: Bamboo
    2016-04-26 07:42:38,877 INFO [localhost-startStop-1] [lifecycle] atlassian.org.osgi.framework.bootdelegation set to javax.servlet,javax.servlet.*,sun.*,com.sun.*,org.w3c.dom.*,org.apache.xerces.*
    2016-04-26 07:42:40,737 INFO [localhost-startStop-1] [lifecycle] Starting Bamboo 5.10.3 (build #51020 Mon Mar 14 14:26:34 UTC 2016) using Java 1.8.0_91 from Oracle Corporation
    2016-04-26 07:42:40,737 INFO [localhost-startStop-1] [lifecycle] Real path of servlet context: /home/bamboo/atlassian-bamboo-5.10.3/atlassian-bamboo/
    2016-04-26 07:42:40,822 INFO [localhost-startStop-1] [DefaultSetupPersister] Current setup step: setupLicense
    2016-04-26 07:42:40,828 INFO [localhost-startStop-1] [lifecycle] Bamboo home directory: /var/atlassian/application/bamboo
    2016-04-26 07:42:40,828 INFO [localhost-startStop-1] [lifecycle] Default charset: UTF-8
    2016-04-26 07:42:40,841 INFO [localhost-startStop-1] [UpgradeLauncher] Upgrades not performed since the application has not been set up yet.

    2016-04-26 07:43:21,900 INFO [localhost-startStop-1] [SessionIdGeneratorBase] Creation of SecureRandom instance for session ID generation using [SHA1PRNG] took [41,050] milliseconds
```

You can make sure the process status.


```
[root@server1 bamboo]# ps aux | grep bamboo
bamboo 21018 88.5 42.7 2705504 432068 ? Sl 05:54 0:20 //bin/java -Djava.util.logging.config.file=/opt/atlassian/bamboo/current/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Xms256m -Xmx384m -Djava.endorsed.dirs=/opt/atlassian/bamboo/current/endorsed -classpath /opt/atlassian/bamboo/current/bin/bootstrap.jar:/opt/atlassian/bamboo/current/bin/tomcat-juli.jar -Dcatalina.base=/opt/atlassian/bamboo/current -Dcatalina.home=/opt/atlassian/bamboo/current -Djava.io.tmpdir=/opt/atlassian/bamboo/current/temp org.apache.catalina.startup.Bootstrap start
root 21041 0.0 0.2 112656 2380 pts/0 S+ 05:54 0:00 grep --color=auto bamboo
```

You can also create an Init script to manage this application.
# 5. Creating Init Script

You can create an init script file  /etc.init.d/bamboo and make it executable. You can place this script inside the init script.


```
[root@server1 bamboo]# cat /etc/init.d/bamboo
#!/bin/sh
set -e
### BEGIN INIT INFO
# Provides: bamboo
# Required-Start: $local_fs $remote_fs $network $time
# Required-Stop: $local_fs $remote_fs $network $time
# Should-Start: $syslog
# Should-Stop: $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Atlassian Bamboo Server
### END INIT INFO
# INIT Script
######################################

# Define some variables
# Name of app ( bamboo, Confluence, etc )
APP=bamboo
# Name of the user to run as
USER=bamboo
# Location of application's bin directory
BASE=/opt/atlassian/bamboo/current

case "$1" in
# Start command
start)
echo "Starting $APP"
/bin/su - $USER -c "export BAMBOO_HOME=${BAMBOO_HOME}; $BASE/bin/startup.sh &> /dev/null"
;;
# Stop command
stop)
echo "Stopping $APP"
/bin/su - $USER -c "$BASE/bin/shutdown.sh &> /dev/null"
echo "$APP stopped successfully"
;;
# Restart command
restart)
$0 stop
sleep 5
$0 start
;;
*)
echo "Usage: /etc/init.d/$APP {start|restart|stop}"
exit 1
;;
esac

exit 0

root@server1 bamboo]# chmod +x /etc/init.d/bamboo
[root@server1 bamboo]# /sbin/chkconfig --add bamboo
[root@server1 bamboo]#
[root@server1 bamboo]# chkconfig --list bamboo

 

bamboo 0:off 1:off 2:on 3:on 4:on 5:on 6:off
```

After starting the application, you can access your Bamboo instance by going to your web browser and entering the address http://45.33.76.60:8085/
# 6. Configure Bamboo

You need to acquire a valid license for the Bamboo installation from their store here. I've took my Bamboo evaluation license and start up with the installation.


![image](https://github.com/nu11secur1ty/Install-Bamboo-on-CentOS-7/blob/master/photo/acquirelicense-1024x576.png)

We need to provide this license information to proceed with the installation. Once the license is provided, you can choose any set-up method according to our preferences for the installation. I choose the Express Installation Method.


![image](https://github.com/nu11secur1ty/Install-Bamboo-on-CentOS-7/blob/master/photo/expressfinalinstall-1024x530.png)


# 7. Setup Administrator User

Now you can create an administrator user to manage the application which is the final installation step.


![image](https://github.com/nu11secur1ty/Install-Bamboo-on-CentOS-7/blob/master/photo/expressinstallation-1024x530.png)

This user will have the global administrative privileges for the entire Bamboo installation and should not be deleted.

Once you've entered these details and click Finish. The Bamboo dashboard will be ready to use.

Congratulations, you have successfully set up Bamboo!


![image](https://github.com/nu11secur1ty/Install-Bamboo-on-CentOS-7/blob/master/photo/Build-Dashboard-Atlassian-Bamboo-2016-04-26-13-22-20-1024x536.png)


Now we've completed with the installations and set-ups. You can start your work with this application!! 

# Have fun with nu11secur1ty =)





